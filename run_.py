from const import RELATIVE
from experiment import Experiment
from data_consts import *


exp = Experiment("data/alex1/indicators.csv")

#
# indicator_plot = exp.plot_impacts([SC_INST_20, SC_INST_30],
#                                   IND_LOP,
#                                   "electricity_production", save=False)
#
# exp.plot_impacts([SC_INCR_30, SC_INCR_40, SC_INCR_50], IND_LOP,
#                  "electricity_production", indicator_plot=indicator_plot)
#
# indicator_plot = exp.plot_impacts(["installed_2020", "installed_2030"], IND_FETP,
#                                   "electricity_production.renewables", RELATIVE, save=False)
#
# exp.plot_impacts([SC_INCR_30], IND_FETP,
#                  "electricity_production.renewables", RELATIVE, indicator_plot=indicator_plot)
#
# # create csv file for the observable sanki diagram
# exp.exporter.d3sanki(SC_INST_20, IND_FETP)
# exp.exporter.d3sanki(SC_INCR_30, IND_LOP)

dfs = exp.prepare_pca_python("energysystem.storage.thermal_storage")
print(len(dfs))
